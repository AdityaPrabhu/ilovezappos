package com.adi.zappos.ilovezappos.rest;

/**
 * Created by adi on 2/8/17.
 */

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ZapposHttpClient {

    public static final String BASE_URL = "https://api.zappos.com/";
    public final static String API_KEY = "b743e26728e16b81da139182bb2094357c31d331";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;

    }
}
