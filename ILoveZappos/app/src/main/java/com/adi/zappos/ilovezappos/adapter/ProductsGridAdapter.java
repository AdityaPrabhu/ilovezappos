package com.adi.zappos.ilovezappos.adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.adi.zappos.ilovezappos.BR;
import com.adi.zappos.ilovezappos.MainActivity;
import com.adi.zappos.ilovezappos.ProductPage;
import com.adi.zappos.ilovezappos.R;
import com.adi.zappos.ilovezappos.databinding.GridviewProductItemBinding;
import com.adi.zappos.ilovezappos.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by adi on 08/02/17.
 */
public class ProductsGridAdapter extends BaseAdapter {

    private MainActivity activity;
    private LayoutInflater inflater;
    private ArrayList<Product> mlist;
    private GridviewProductItemBinding binding;

    public ProductsGridAdapter(MainActivity activity, ArrayList<Product> products) {

        this.activity = activity;
        this.mlist = products;
        inflater = LayoutInflater.from(activity);

    }

    @Override
    public int getCount() {
        return mlist == null ? 0 : mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        /*Data binding done with the child item of the gridview*/
        if (convertView == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.gridview_product_item, parent, false);
            convertView = binding.getRoot();
            convertView.setTag(binding);
        } else {
            binding = (GridviewProductItemBinding) convertView.getTag();
        }
        binding.setVariable(BR.item, mlist.get(position));
        binding.executePendingBindings();

        /*Download the image using Picasso*/
        ImageView imageView = (ImageView) convertView.findViewById(R.id.product_thumbnail);
        String url = mlist.get(position).getThumbnailImageUrl();
        Picasso.with(activity)
                .load(url)
                .placeholder(R.mipmap.ic_placeholder)
                .error(R.mipmap.ic_error_fallback)
                .into(imageView);

        /*Pass the Product object to the PorductPage Activity. For this the object is serialized using Parcelable Interface*/
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = mlist.get(position);
                Intent intent = new Intent(activity, ProductPage.class);
                intent.putExtra("com.adi.zappos.ilovezappos.model.Product", product);
                activity.startActivity(intent);
            }
        });

        return convertView;

    }

}
