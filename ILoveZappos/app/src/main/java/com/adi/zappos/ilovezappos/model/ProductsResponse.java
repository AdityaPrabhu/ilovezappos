package com.adi.zappos.ilovezappos.model;

/**
 * Created by adi on 2/8/17.
 */

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ProductsResponse {

    /*The response of the HTTP request using Retrofit is captured by an object of this class*/

    @SerializedName("originalTerm")
    private String originalTerm;
    @SerializedName("currentResultCount")
    private String currentResultCount;
    @SerializedName("totalResultCount")
    private String totalResultCount;
    @SerializedName("term")
    private String term;
    @SerializedName("results")
    private ArrayList<Product> results;
    @SerializedName("statusCode")
    private String statusCode;

    public String getOriginalTerm() {
        return originalTerm;
    }

    public String getCurrentResultCount() {
        return currentResultCount;
    }

    public String getTotalResultCount() {
        return totalResultCount;
    }

    public String getTerm() {
        return term;
    }

    public ArrayList<Product> getResults() {
        return results;
    }

    public String getStatusCode() {
        return statusCode;
    }

}