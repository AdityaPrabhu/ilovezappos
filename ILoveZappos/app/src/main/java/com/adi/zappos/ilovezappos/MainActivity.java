package com.adi.zappos.ilovezappos;

/**
 * Created by adi on 2/8/17.
 */

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import com.adi.zappos.ilovezappos.model.Product;
import com.adi.zappos.ilovezappos.adapter.ProductsGridAdapter;
import com.adi.zappos.ilovezappos.model.ProductsResponse;
import com.adi.zappos.ilovezappos.rest.ZapposHttpClient;
import com.adi.zappos.ilovezappos.rest.ZapposHttpClientInterface;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private MainActivity activity;

    private Toolbar myToolbar;
    private ArrayList<Product> products;
    public ProductsGridAdapter productsGridAdapter;

    private GridView gridView;
    private ProgressBar spinner;

    private String searchQuery;

    private static final String STATE_ITEMS = "products";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*Retrieve saved state during configuration change */
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            products = (ArrayList<Product>) savedInstanceState.getSerializable(STATE_ITEMS);
        }

        this.activity = this;
        setContentView(R.layout.activity_main);
        setToolbar();

        handleIntent(getIntent()); //the intent of Search query

        setGridView(); //set the gridview of the activity

        setSpinner(); //set the progress bar

        fetchDataFromServer(); //fetch the data from a Zappos server

    }

    private void fetchDataFromServer() {

        //Set up Retrofit client
        ZapposHttpClientInterface apiService =
                ZapposHttpClient.getClient().create(ZapposHttpClientInterface.class);

        Call<ProductsResponse> call = apiService.getProducts(searchQuery, ZapposHttpClient.API_KEY);
        call.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                spinner.setVisibility(View.GONE);
                if (searchQuery == null)
                    searchQuery = getString(R.string.app_name);
                myToolbar.setTitle(searchQuery);
                products = response.body().getResults();
                //Log.d(TAG, "Products fetched: " + products.size());
                productsGridAdapter = new ProductsGridAdapter(activity, products);
                gridView.setAdapter(productsGridAdapter);

            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {

                spinner.setVisibility(View.GONE);
                Log.e(TAG, t.toString());

            }
        });

    }

    private void setSpinner() {

        spinner = (ProgressBar) findViewById(R.id.fetch_products_progress);
        spinner.setVisibility(View.VISIBLE);

    }

    private void setGridView() {

        gridView = (GridView) findViewById(R.id.gridview_products);
        gridView.setBackgroundColor(Color.WHITE);
        gridView.setVerticalSpacing(5);
        gridView.setHorizontalSpacing(5);

    }

    private void setToolbar() {

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        //Fetch data if intent.action matches
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            searchQuery = intent.getStringExtra(SearchManager.QUERY);
            //Log.d(TAG, searchQuery);
            setSpinner();
            fetchDataFromServer();

        }
    }

    /*Save state for handling configuring changes*/
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_ITEMS, products);

    }
}


