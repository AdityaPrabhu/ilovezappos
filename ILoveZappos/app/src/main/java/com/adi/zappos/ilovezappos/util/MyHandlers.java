package com.adi.zappos.ilovezappos.util;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.adi.zappos.ilovezappos.ProductPage;
import com.adi.zappos.ilovezappos.R;

/**
 * Created by adi on 2/9/17.
 */
public class MyHandlers {

    private Context context;
    private ImageView box, cart;

    public MyHandlers(Context context, ImageView box, ImageView cart) {
        this.context = context;
        this.box = box;
        this.cart = cart;
    }

    /*On click of the shopping cart floating actin button. This method is data binded */
    public void onClickedPurchased(final View view) {

        int fromLoc[] = new int[2];
        box.getLocationOnScreen(fromLoc);
        float startX = fromLoc[0];
        float startY = fromLoc[1];

        int toLoc[] = new int[2];
        cart.getLocationOnScreen(toLoc);
        float destX = toLoc[0];
        float destY = toLoc[1];

        //System.out.println("start: " + startX + " " + startY);
        //System.out.println("dest: " + destX + " " + destY);

        /*Set the Animation Listener */
        Animation.AnimationListener animL = new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                ((ProductPage) context).isAddedtoCart = true;
                view.setClickable(false);
                ((FloatingActionButton) view).setImageResource(R.mipmap.buy_grayed);
                box.setVisibility(View.GONE);
                ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                scale.setDuration(500);
                scale.setInterpolator(new OvershootInterpolator());
                cart.startAnimation(scale);
                cart.setBackgroundResource(R.mipmap.shopping_cart_loaded_white);
                Toast.makeText(context, "Added to cart", Toast.LENGTH_SHORT).show();

            }
        };

        //Do the animation
        MyAnimations anim = new MyAnimations();
        Animation a = anim.fromAtoB(0, 0, destX - startX, destY - startY, animL, 1500);
        box.startAnimation(a);

    }
}
