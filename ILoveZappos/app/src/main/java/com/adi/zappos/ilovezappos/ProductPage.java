package com.adi.zappos.ilovezappos;

/**
 * Created by adi on 2/8/17.
 */

import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adi.zappos.ilovezappos.databinding.ActivityProductPageBinding;
import com.adi.zappos.ilovezappos.model.Product;
import com.adi.zappos.ilovezappos.util.MyHandlers;
import com.squareup.picasso.Picasso;

public class ProductPage extends AppCompatActivity {

    private static final String STATE_ITEM = "product";
    private Product product;
    private static final String STATE_BOOLEAN = "is_added";
    public boolean isAddedtoCart;

    private ActivityProductPageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*Retrieve saved state during configuration change */
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            product = (Product) savedInstanceState.getSerializable(STATE_ITEM);
            isAddedtoCart = savedInstanceState.getBoolean(STATE_BOOLEAN);
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_page);

        setToolBar();

        //Retrive passed Product object from MainActivity
        Bundle bundle = getIntent().getExtras();
        product = bundle.getParcelable("com.adi.zappos.ilovezappos.model.Product");

        //data bind the product object
        binding.setProduct(product);

        setPriceViews();

        setUI();

    }

    /*Set all ImageView and the handler of the FloatingActionButton */
    private void setUI() {

        ImageView imageView = (ImageView) findViewById(R.id.product_image);
        String url = product.getThumbnailImageUrl();
        Picasso.with(this)
                .load(url)
                .placeholder(R.mipmap.ic_placeholder)
                .error(R.mipmap.ic_error_fallback)
                .into(imageView);
        ImageView box = (ImageView) findViewById(R.id.box);
        ImageView cart = (ImageView) findViewById(R.id.shopping_cart);
        FloatingActionButton buyButton = (FloatingActionButton) findViewById(R.id.buy_button);
        if (isAddedtoCart) {
            box.setVisibility(View.GONE);
            cart.setBackgroundResource(R.mipmap.shopping_cart_loaded_white);
            buyButton.setClickable(false);
            buyButton.setImageResource(R.mipmap.buy_grayed);
        }

        //Create and data bind the handler
        MyHandlers handlers = new MyHandlers(this, box, cart);
        binding.setHandlers(handlers);

    }

    /*Handle price views if there is a discount*/
    private void setPriceViews() {

        double val1 = Double.valueOf((product.getOriginalPrice()).substring(1));
        double val2 = Double.valueOf((product.getPrice()).substring(1));
        TextView originalPriceTV = (TextView) findViewById(R.id.original_price_tv);
        if (val1 == val2) {
            originalPriceTV.setVisibility(View.GONE);
        } else {
            originalPriceTV.setVisibility(View.VISIBLE);
            originalPriceTV.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        }

    }

    private void setToolBar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);

    }

    /*Save state for handling configuring changes*/
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_ITEM, product);
        outState.putBoolean(STATE_BOOLEAN, isAddedtoCart);

    }

}
