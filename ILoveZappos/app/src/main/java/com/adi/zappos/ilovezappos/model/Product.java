package com.adi.zappos.ilovezappos.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by adi on 2/8/17.
 */
public class Product extends BaseObservable implements Parcelable {

    /*The member variables are serialized for the sake of passing objects between Activities.
        The serialized names should be in accordance with Retrofit guidelines*/
    @SerializedName("brandName")
    private String brandName;
    @SerializedName("thumbnailImageUrl")
    private String thumbnailImageUrl;
    @SerializedName("productId")
    private String productId;
    @SerializedName("originalPrice")
    private String originalPrice;
    @SerializedName("styleId")
    private String styleId;
    @SerializedName("colorId")
    private String colorId;
    @SerializedName("price")
    private String price;
    @SerializedName("percentOff")
    private String percentOff;
    @SerializedName("productUrl")
    private String productUrl;
    @SerializedName("productName")
    private String productName;

    public Product(String brandName, String thumbnailImageUrl, String productId, String originalPrice, String styleId, String colorId, String price, String percentOff, String productUrl, String productName) {

        this.brandName = brandName;
        this.thumbnailImageUrl = thumbnailImageUrl;
        this.productId = productId;
        this.originalPrice = originalPrice;
        this.styleId = styleId;
        this.colorId = colorId;
        this.price = price;
        this.percentOff = percentOff;
        this.productUrl = productUrl;

        this.productName = productName;
    }

    public Product(Parcel in) {

        this.brandName = in.readString();
        this.thumbnailImageUrl = in.readString();
        this.productId = in.readString();
        this.originalPrice = in.readString();
        this.styleId = in.readString();
        this.colorId = in.readString();
        this.price = in.readString();
        this.percentOff = in.readString();
        this.productUrl = in.readString();
        this.productName = in.readString();

    }

    public String getBrandName() {
        return brandName;
    }

    public String getThumbnailImageUrl() {
        return thumbnailImageUrl;
    }

    public String getProductId() {
        return productId;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public String getStyleId() {
        return styleId;
    }

    public String getColorId() {
        return colorId;
    }

    public String getPrice() {
        return price;
    }

    public String getPercentOff() {
        return percentOff;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setThumbnailImageUrl(String thumbnailImageUrl) {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPercentOff(String percentOff) {
        this.percentOff = percentOff;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(brandName);
        dest.writeString(thumbnailImageUrl);
        dest.writeString(productId);
        dest.writeString(originalPrice);
        dest.writeString(styleId);
        dest.writeString(colorId);
        dest.writeString(price);
        dest.writeString(percentOff);
        dest.writeString(productUrl);
        dest.writeString(productName);

    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {

        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }

    };
}
