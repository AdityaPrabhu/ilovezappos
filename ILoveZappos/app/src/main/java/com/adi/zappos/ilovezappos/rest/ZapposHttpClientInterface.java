package com.adi.zappos.ilovezappos.rest;

/**
 * Created by adi on 2/8/17.
 */

import com.adi.zappos.ilovezappos.model.ProductsResponse;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ZapposHttpClientInterface {

    /*The GET request using Retrofit*/

    @GET("Search")
    Call<ProductsResponse> getProducts(@Query("term") String term, @Query("key") String key);

}